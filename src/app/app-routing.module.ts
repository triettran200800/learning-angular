import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
    path: "feature",
    loadChildren: () =>
      import("./feature-a/feature-a.module").then((m) => m.FeatureAModule),
  },
  {
    path: "heroes",
    loadChildren: () =>
      import("./modules/heroes/heroes.module").then((m) => m.HeroesModule),
  },
  {
    path: "",
    redirectTo: "feature",
    pathMatch: "full",
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
