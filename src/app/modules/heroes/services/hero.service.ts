import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { Hero } from "../models/hero";
import { HEROES } from "../models/mock-heroes";
import { Observable, of } from "rxjs";
import { catchError, map, tap } from "rxjs/operators";

import { MessageService } from "app/core/services/message.service";

@Injectable({
  providedIn: "root",
})
export class HeroService {
  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) {}

  private heroesUrl = "api/heroes"; // URL to web api

  getHeroes(): Observable<Hero[]> {
    const heroes = of(HEROES);
    // return this.http.get<Hero[]>(this.heroesUrl).pipe(
    //   tap((_) => this.log("fetched heroes")),
    //   catchError(this.handleError<Hero[]>("getHeroes", []))
    // );

    return heroes;
  }

  getHero(id: number): Observable<Hero> {
    return of(HEROES.find((hero) => hero.id === id));
  }

  private log(message: string) {
    this.messageService.add(`HeroService: ${message}`);
  }
  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
