import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "@shared/shared.module";

import { HeroesRoutingModule } from "./heroes-routing.module";

import { HeroesComponent } from "./pages/heroes/heroes.component";
import { HeroDetailComponent } from "./pages/hero-detail/hero-detail.component";

@NgModule({
  declarations: [HeroesComponent, HeroDetailComponent],
  imports: [CommonModule, SharedModule, HeroesRoutingModule],
})
export class HeroesModule {}
